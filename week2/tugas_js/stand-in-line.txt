function nextInLine(arr, item) {
  // Only change code below this line
  if(arr.length > 0 && item != 10) {
    return arr[0];
  }
  else if(arr[4] == 5) {
    arr[4] += 5;
    
    return arr[4];
  }
  return item;
  // Only change code above this line
  

}

// Setup
var testArr = [1,2,3,4,5];

// Display code
console.log("Before: " + JSON.stringify(testArr));
console.log(nextInLine(testArr, 6));
console.log("After: " + JSON.stringify(testArr));
